﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicNewtonBall : MonoBehaviour
{

    Vector2 mousePosition;
    
    void CheckCollisionWithBall()
    {
        
        mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

        RaycastHit2D hitObject = Physics2D.Raycast(mousePosition, Vector2.zero, 0.0f);

        if(hitObject)
        {
            hitObject.collider.gameObject.transform.position = mousePosition;
        }

    }


    void Update()
    {
      if(Input.GetMouseButton(0))
        {
            CheckCollisionWithBall();
        }
    }
}
