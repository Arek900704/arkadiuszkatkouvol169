﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUpdate : MonoBehaviour {

    public TowerLogic towerLogic;
    public Text text;
	// Use this for initialization
	
	
	// Update is called once per frame
	void Update () {
        text.text = string.Concat("Towers: ", towerLogic.towerCount);
	}
}
