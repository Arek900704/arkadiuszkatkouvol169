﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerLogic : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject towerPrefab;
    public int towerCount = 0;
    TowerBehavoir towerbehavior;
    bool resetAmmo = true;

    // Update is called once per frame
    void Update()
    {
        towerCount = GameObject.FindGameObjectsWithTag("Tower").Length;

        foreach (GameObject tower in GameObject.FindGameObjectsWithTag("Tower"))
        {
            towerbehavior = tower.GetComponent<TowerBehavoir>();

            switch (towerbehavior.current_behawoir)
            {
                case 0:
                    {
                        towerbehavior.timer += Time.deltaTime;

                        if (towerbehavior.timer >= 6.0f)
                        {
                            towerbehavior.timer = 0;
                            towerbehavior.current_behawoir = 1;
                            tower.GetComponent<SpriteRenderer>().color = Color.red;
                        }
                        break;
                    }


                case 1:
                    {
                        if (towerbehavior.ammo == 0)
                        {
                            tower.GetComponent<SpriteRenderer>().color = Color.white;
                            towerbehavior.current_behawoir = 2;
                            break;
                        }

                        towerbehavior.timer += Time.deltaTime;

                        if (towerbehavior.timer > 0.5f)
                        {
                            towerbehavior.timer = 0;
                            tower.transform.Rotate(new Vector3(0.0f, 0.0f, Random.Range(15.0f, 45.0f)));
                            bulletPrefab.transform.position = new Vector3(tower.transform.position.x, tower.transform.position.y + 0.32f);
                            bulletPrefab.transform.rotation = new Quaternion(tower.transform.rotation.x, tower.transform.rotation.y, tower.transform.rotation.z, tower.transform.rotation.w);
                            bulletPrefab.GetComponent<BulletBehavior>().start_position = new Vector3(tower.transform.position.x, tower.transform.position.y + 0.32f);
                            bulletPrefab.GetComponent<BulletBehavior>().distance = Random.Range(1.0f, 4.0f);
                            Instantiate(bulletPrefab);
                            towerbehavior.ammo--;
                        }

                        break;
                    }

                case 2:
                    {
                        break;
                    }
            }
        }

        foreach (GameObject bullet in GameObject.FindGameObjectsWithTag("Bullet"))
        {

            bullet.transform.Translate(Vector3.up * (4.0f) * Time.deltaTime);

            if (Vector2.Distance(bullet.GetComponent<BulletBehavior>().start_position, bullet.transform.position) >= bullet.GetComponent<BulletBehavior>().distance)
            {
                if (towerCount < 100 && resetAmmo)
                {
                    towerPrefab.transform.position = bullet.transform.position;
                    Instantiate(towerPrefab);
                }

                Destroy(bullet);
            }


        }

        if (towerCount == 100 && resetAmmo)
        {
            foreach (GameObject tower in GameObject.FindGameObjectsWithTag("Tower"))
            {
                towerbehavior = tower.GetComponent<TowerBehavoir>();
                towerbehavior.ammo = 12;
                towerbehavior.current_behawoir = 1;
                tower.GetComponent<SpriteRenderer>().color = Color.red;
                resetAmmo = false;
            }
        } 
    }

    public int returnTowers()
    {
        return towerCount;
    }
}
